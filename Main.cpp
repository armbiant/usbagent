/**
 * This file is a part of DesQ Disks.
 * DesQ Disks is the Disks Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

// Local Headers
#include "Global.hpp"
#include "Disks.hpp"

#include <desq/desq-config.h>
#include <desq/Utils.hpp>

#include <DFL/DF5/Application.hpp>
#include <DFL/DF5/Storage.hpp>
#include <DFL/DF5/Utils.hpp>
#include <DFL/DF5/Xdg.hpp>

DFL::Settings         *disksSett;
DFL::Storage::Manager *diskMgr;

int main( int argc, char **argv ) {
#if (QT_VERSION >= QT_VERSION_CHECK( 5, 6, 0 ) )
        QApplication::setAttribute( Qt::AA_EnableHighDpiScaling );
#endif

    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Disks.log" ).toLocal8Bit().data(), "a" );
    qInstallMessageHandler( DFL::Logger );

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ Disks started at" << QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8().constData();
    qDebug() << "------------------------------------------------------------------------";

    DFL::Application *app = new DFL::Application( argc, argv );

    app->setOrganizationName( "DesQ" );
    app->setApplicationName( "Disks" );
    app->setApplicationVersion( PROJECT_VERSION );
    app->setQuitOnLastWindowClosed( false );

    if ( app->isRunning() ) {
        app->messageServer( "activate-window" );
        return 0;
    }

    if ( app->lockApplication() ) {
        /* Settings Instance */
        disksSett = new DFL::Settings( "DesQ", "Disks", ConfigPath );

        /* StorageManager */
        diskMgr = DFL::Storage::Manager::instance();

        DesQ::Disks::UI *disks = new DesQ::Disks::UI();

        disks->startTray();

        return app->exec();
    }

    return 0;
}
