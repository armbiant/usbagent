/**
 * This file is a part of DesQ Disks.
 * DesQ Disks is the Disks Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "DriveWidget.hpp"

DesQ::Disks::Partition::Partition( DFL::Storage::Block block, QWidget *parent ) : QWidget( parent ) {
    mBlock = block;

    setFixedHeight( 36 );

    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setContentsMargins( QMargins( 20, 0, 0, 0 ) );

    QLabel *icon = new QLabel();

    icon->setFixedSize( QSize( 36, 36 ) );
    icon->setPixmap( QIcon::fromTheme( "drive-partition" ).pixmap( 28 ) );
    icon->setFixedHeight( 36 );

    QLabel *label = new QLabel();

    label->setText( block.label().isEmpty() ? block.device() : block.label() );

    usage = new QProgressBar();
    usage->setRange( 0, 100 );
    usage->setValue( 0 );

    QVBoxLayout *lblLyt = new QVBoxLayout();

    lblLyt->addWidget( label );
    lblLyt->addWidget( usage );
    usage->hide();

    if ( block.mountPoint().count() ) {
        usage->setValue( 100.0 - 1.0 * block.availableSize() / block.totalSize() * 100 );
        usage->show();
    }

    mtBtn = new QToolButton();
    mtBtn->setAutoRaise( true );
    mtBtn->setFixedSize( QSize( 36, 36 ) );
    mtBtn->setFocusPolicy( Qt::NoFocus );
    mtBtn->setIconSize( QSize( 16, 16 ) );
    mtBtn->setIcon( QIcon::fromTheme( block.mountPoint().isEmpty() ? "media-mount" : "media-eject" ) );

    openBtn = new QToolButton();
    openBtn->setAutoRaise( true );
    openBtn->setFixedSize( QSize( 36, 36 ) );
    openBtn->setFocusPolicy( Qt::NoFocus );
    openBtn->setIconSize( QSize( 16, 16 ) );
    openBtn->setIcon( QIcon::fromTheme( "folder" ) );
    openBtn->setToolTip( "Click to open" );

    lyt->addWidget( icon );
    lyt->addLayout( lblLyt );
    lyt->addStretch();
    lyt->addWidget( openBtn );
    lyt->addWidget( mtBtn );

    setLayout( lyt );

    connect( mtBtn, &QToolButton::clicked, this, &DesQ::Disks::Partition::mountUnmount );
    connect(
        openBtn, &QToolButton::clicked, [ = ] () {
            QProcess::startDetached( "xdg-open", { mBlock.mountPoint() } );
        }
    );

    /* Mount only if */
    DFL::Storage::Device dev( QFileInfo( mBlock.drive() ).baseName() );

    if ( not dev.isRemovable() and not dev.isOptical() ) {
        mtBtn->setDisabled( true );
    }

    if ( not mBlock.mountPoint().count() ) {
        openBtn->hide();
    }

    /** Try to mount if not mounted. */
    if ( disksSett->value( "AutoMount" ) ) {
        QTimer::singleShot(
            1000, [ = ] () {
                /* Return if mounted */
                if ( mBlock.mountPoint().count() ) {
                    return;
                }

                mountUnmount();
            }
        );
    }

    /* If not AutoMounting, see if we have to ask the user whether to mount */
    else if ( disksSett->value( "AskToMount" ) ) {
        // QTimer::singleShot(
        //     1000, [ = ] () {
        //         /* Return if mounted */
        //         if ( mBlock.mountPoint().count() ) {
        //             return;
        //         }
        //
        //         mountUnmount();
        //     }
        // );
    }
}


DesQ::Disks::Partition::~Partition() {
    delete mtBtn;
}


void DesQ::Disks::Partition::mountUnmount() {
    /* Mount this partition */
    if ( mBlock.mountPoint().isEmpty() ) {
        /* Return if it does not have a valid file system */
        if ( not mBlock.fileSystem().count() ) {
            return;
        }

        /* Mount only if it's removable */
        DFL::Storage::Device dev( QFileInfo( mBlock.drive() ).baseName() );

        if ( not dev.isRemovable() and not dev.isOptical() ) {
            return;
        }

        if ( mBlock.mount() ) {
            mtBtn->setIcon( QIcon::fromTheme( "media-eject" ) );
            usage->setValue( 1.0 * mBlock.availableSize() / mBlock.totalSize() * 100 );
            usage->show();
            openBtn->show();

            /**
             * Wait upto 5s for the mount point to become ready.
             */
            int t = 0;
            while ( not QFileInfo( mBlock.mountPoint() ).exists() ) {
                QThread::usleep( 500 );
                qApp->processEvents();

                t += 500;

                if ( t >= 5 * 1000 * 1000 ) {
                    break;
                }
            }

            /* Notify the user */
            if ( disksSett->value( "NotifyOnMount" ) ) {
                QProcess::startDetached(
                    "notify-send", {
                        "-i", "desq-disks",
                        "-a", "DesQ Disks",
                        "-u", "normal",
                        "Device Mounter",
                        "The device " + mBlock.device() + " has been mounted at <tt>" +
                        mBlock.mountPoint() + "</tt>"
                    }
                );
            }

            /* Open */
            if ( disksSett->value( "OpenOnMount" ) ) {
                QProcess::startDetached( "xdg-open", { mBlock.mountPoint() } );
            }
        }
    }

    else {
        if ( mBlock.unmount() ) {
            mtBtn->setIcon( QIcon::fromTheme( "media-mount" ) );
            usage->setValue( 0 );
            usage->hide();
            openBtn->hide();
        }
    }
}


DesQ::Disks::Drive::Drive( DFL::Storage::Device stDev, QWidget *parent ) : QWidget( parent ) {
    this->stDev = stDev;
    mFolded     = false;

    /* Base Layout */
    baseLyt = new QVBoxLayout();
    baseLyt->setContentsMargins( QMargins( 2, 2, 2, 2 ) );

    /* Drive */
    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setContentsMargins( QMargins() );

    QLabel *icon = new QLabel();

    icon->setFixedSize( QSize( 36, 36 ) );
    icon->setAlignment( Qt::AlignCenter );

    if ( stDev.isOptical() ) {
        icon->setPixmap( QIcon::fromTheme( "drive-optical" ).pixmap( 28 ) );
    }

    if ( stDev.isRemovable() ) {
        icon->setPixmap( QIcon::fromTheme( "drive-removable-media" ).pixmap( 28 ) );
    }

    else {
        icon->setPixmap( QIcon::fromTheme( "drive-harddisk" ).pixmap( 28 ) );
    }

    label = new QLabel();
    label->setText( stDev.label().isEmpty() ? QFileInfo( stDev.path() ).baseName() : stDev.label() );
    label->setFixedHeight( 36 );

    QPushButton *ejectBtn = new QPushButton( QIcon::fromTheme( "media-eject" ), "Safely Remove" );

    ejectBtn->setIconSize( QSize( 16, 16 ) );
    ejectBtn->setFocusPolicy( Qt::NoFocus );
    ejectBtn->setFlat( true );
    connect(
        ejectBtn, &QToolButton::clicked, [ = ]() mutable {
            bool ok = true;
            for ( DFL::Storage::Block blk: stDev.validPartitions() ) {
                /* If this partiton is mounted, unmount it */
                if ( blk.mountPoint().count() ) {
                    ok &= blk.unmount();
                }
            }

            /*
             * If we were able to unmount all the mounted partitions
             * power-down the drive
             */
            if ( ok ) {
                QProcess::startDetached(
                    "udisksctl", {
                        "power-off",
                        "-b",
                        stDev.partitions().value( 0 ).device().left( 8 )
                    }
                );
            }
        }
    );

    lyt->addWidget( icon );
    lyt->addWidget( label );
    lyt->addStretch();
    lyt->addWidget( ejectBtn );

    if ( not (stDev.isRemovable() or stDev.isOptical() ) ) {
        ejectBtn->setDisabled( true );
        ejectBtn->hide();
    }

    baseLyt->addLayout( lyt );
    setLayout( baseLyt );

    update();
}


DesQ::Disks::Drive::~Drive() {
    for ( DesQ::Disks::Partition *part: parts ) {
        delete part;
    }

    parts.clear();
}


void DesQ::Disks::Drive::update() {
    /** Remove the existing partition widgets */
    for ( DesQ::Disks::Partition *part: parts ) {
        baseLyt->removeWidget( part );
        delete part;
    }

    parts.clear();

    /** Add the newly created partition widgets */
    for ( DFL::Storage::Block blk: stDev.validPartitions() ) {
        DesQ::Disks::Partition *part = new DesQ::Disks::Partition( blk, this );
        baseLyt->addWidget( part );

        parts << part;
    }

    /* Calculate height */
    int height = baseLyt->contentsMargins().top() + baseLyt->contentsMargins().bottom();

    height += (parts.count() + 1) * 36 + parts.count() * baseLyt->spacing();

    setFixedHeight( height );
}


void DesQ::Disks::Drive::mouseReleaseEvent( QMouseEvent *mEvent ) {
    if ( mEvent->button() == Qt::LeftButton ) {
        /* If the label was clicked */
        int y = mEvent->pos().y();

        if ( (y > baseLyt->contentsMargins().top() ) and y < (baseLyt->contentsMargins().top() + 36) ) {
            int height = baseLyt->contentsMargins().top() + baseLyt->contentsMargins().bottom();

            if ( not mFolded ) {
                for ( DesQ::Disks::Partition *part: parts ) {
                    part->hide();
                }

                setFixedHeight( height + 36 );
            }

            else {
                for ( DesQ::Disks::Partition *part: parts ) {
                    part->show();
                }

                /* Calculate height */
                height += (parts.count() + 1) * 36 + parts.count() * baseLyt->spacing();
                setFixedHeight( height );
            }

            mFolded = not mFolded;
        }
    }
}


void DesQ::Disks::Drive::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHint( QPainter::Antialiasing );
    QColor bg( palette().color( QPalette::Highlight ) );

    bg.setAlphaF( 0.05 );
    painter.setPen( palette().color( QPalette::Highlight ) );
    painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 3.0, 3.0 );
    painter.end();

    QWidget::paintEvent( pEvent );
}
