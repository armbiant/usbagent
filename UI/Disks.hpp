/**
 * This file is a part of DesQ Disks.
 * DesQ Disks is the Disks Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"

namespace DesQ {
    namespace Disks {
        class Drive;
        class UI;
    }
}

class DesQ::Disks::UI : public QScrollArea {
    Q_OBJECT;

    public:
        UI();

        void startTray();

    private:
        void handleDeviceAdded( QString );
        void handleDeviceRemoved( QString );

        void handlePartitionAdded( QString );
        void handlePartitionRemoved( QString );

        void buildUI();
        void setWindowProperties();

        void reload();

        QMap<QString, DesQ::Disks::Drive *> drives;
        QMap<QString, QString> parts;

        QVBoxLayout *baseLyt;

    protected:
        void resizeEvent( QResizeEvent *rEvent );
};
