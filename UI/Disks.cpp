/**
 * This file is a part of DesQ Disks.
 * DesQ Disks is the Disks Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Disks.hpp"
#include "DriveWidget.hpp"

#include <DFL/DF5/Storage.hpp>

DesQ::Disks::UI::UI() : QScrollArea() {
    connect( diskMgr, &DFL::Storage::Manager::deviceAdded,      this, &DesQ::Disks::UI::handleDeviceAdded );
    connect( diskMgr, &DFL::Storage::Manager::deviceRemoved,    this, &DesQ::Disks::UI::handleDeviceRemoved );

    connect( diskMgr, &DFL::Storage::Manager::partitionAdded,   this, &DesQ::Disks::UI::handlePartitionAdded );
    connect( diskMgr, &DFL::Storage::Manager::partitionRemoved, this, &DesQ::Disks::UI::handlePartitionRemoved );

    QShortcut *dismiss   = new QShortcut( QKeySequence( Qt::Key_Escape ), this );
    QShortcut *reloadAct = new QShortcut( QKeySequence( Qt::Key_F5 ), this );

    connect( dismiss,   &QShortcut::activated, this, &DesQ::Disks::UI::close );
    connect( reloadAct, &QShortcut::activated, this, &DesQ::Disks::UI::reload );

    buildUI();
    setWindowProperties();
}


void DesQ::Disks::UI::startTray() {
    QSystemTrayIcon *icon = new QSystemTrayIcon( QIcon( ":/icons/desq-disks.png" ), this );

    icon->show();

    connect(
        icon, &QSystemTrayIcon::activated, [ = ]( QSystemTrayIcon::ActivationReason reason ) {
            switch ( reason ) {
                    case QSystemTrayIcon::Trigger: {
                        if ( isVisible() ) {
                            hide();
                        }

                        else {
                            show();
                        }

                        break;
                    }

                    case QSystemTrayIcon::Context: {
                        break;
                    }

                    case QSystemTrayIcon::MiddleClick: {
                        qApp->quit();
                        break;
                    }

                    case QSystemTrayIcon::DoubleClick: {
                        break;
                    }

                    case QSystemTrayIcon::Unknown: {
                        break;
                    }
            }
        }
    );
}


void DesQ::Disks::UI::handleDeviceAdded( QString dev ) {
    DFL::Storage::Device drive( dev );

    if ( not drives.contains( dev ) ) {
        DesQ::Disks::Drive *drvWidget = new DesQ::Disks::Drive( drive, nullptr );
        baseLyt->addWidget( drvWidget );
        drives[ dev ] = drvWidget;
    }

    else {
        drives[ dev ]->update();
    }

    widget()->updateGeometry();
}


void DesQ::Disks::UI::handleDeviceRemoved( QString dev ) {
    DesQ::Disks::Drive *drive = drives.take( dev );

    if ( drive ) {
        baseLyt->removeWidget( drive );
        delete drive;
    }

    else {
        return;
    }

    QProcess::startDetached(
        "notify-send", {
            "-i", "desq-disks",
            "-a", "DesQ Disks",
            "-u", "normal",
            "Device Removed",
            "Device removed: " + dev
        }
    );

    widget()->updateGeometry();
}


void DesQ::Disks::UI::handlePartitionAdded( QString dev ) {
    DFL::Storage::Block blk( dev );

    if ( drives.contains( QFileInfo( blk.drive() ).baseName() ) ) {
        drives[ QFileInfo( blk.drive() ).baseName() ]->update();
    }
}


void DesQ::Disks::UI::handlePartitionRemoved( QString dev ) {
    if ( parts.contains( dev ) ) {
        parts.remove( dev );
    }
}


void DesQ::Disks::UI::buildUI() {
    baseLyt = new QVBoxLayout();

    for ( DFL::Storage::Device device: diskMgr->devices() ) {
        QString            devName( QFileInfo( device.path() ).baseName() );
        DesQ::Disks::Drive *drive = new DesQ::Disks::Drive( device, nullptr );
        baseLyt->addWidget( drive );

        drives[ devName ] = drive;
        for ( DFL::Storage::Block blk: device.validPartitions() ) {
            parts[ QFileInfo( blk.device() ).baseName() ] = devName;
        }
    }
    baseLyt->addStretch();

    QWidget *base = new QWidget();

    base->setLayout( baseLyt );
    setWidget( base );
}


void DesQ::Disks::UI::setWindowProperties() {
    setWindowIcon( QIcon( ":/icons/desq-disks.png" ) );
    setWindowTitle( "DesQ Disks Manager" );

    setMinimumSize( 400, 300 );
    setWidgetResizable( true );
    setAlignment( Qt::AlignCenter );

    setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
}


void DesQ::Disks::UI::reload() {
    /** Remove added drives */
    for ( QString dev: drives.keys() ) {
        DesQ::Disks::Drive *drive = drives.take( dev );
        delete drive;
    }

    /** Clear the partition-drive map */
    parts.clear();

    for ( DFL::Storage::Device device: diskMgr->devices() ) {
        QString            devName( QFileInfo( device.path() ).baseName() );
        DesQ::Disks::Drive *drive = new DesQ::Disks::Drive( device, nullptr );
        baseLyt->insertWidget( baseLyt->count() - 1, drive );

        drives[ devName ] = drive;
        for ( DFL::Storage::Block blk: device.validPartitions() ) {
            parts[ QFileInfo( blk.device() ).baseName() ] = devName;
        }
    }
}


void DesQ::Disks::UI::resizeEvent( QResizeEvent *rEvent ) {
    rEvent->accept();
    widget()->setFixedWidth( viewport()->width() );
}
